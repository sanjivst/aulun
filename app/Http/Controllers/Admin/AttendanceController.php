<?php

namespace App\Http\Controllers\Admin;
use Carbon\Carbon;
use App\User;
use App\Attendance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {

        return view('admin.attendance.fill-form-attendance', compact('id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('updated_at', 'desc')->get();
        return view('admin.attendance.register', compact('users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Attendance $attendance, $id)
    {        
        $attendance->user_id = $id;
        $attendance->date = $request->date;
        $attendance->entry_time = $request->entry_time;
        $attendance->exit_time = $request->exit_time;
        
        $time1 = Carbon::parse($attendance->entry_time);
        $time2 = Carbon::parse($attendance->exit_time);

        $time3 = $time2->diffInSeconds($time1);
        $time3 = Carbon::parse($time3);
        
        
        $attendance->working_time = $time3;
        $attendance->save();
        return redirect()->route('attendance.list', $id)->with('success', 'Attendance of User Stored Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function attendanceList($id)
    {
        $attendances = Attendance::where('user_id', $id)->orderBy('updated_at', 'desc')->get();
        $user = User::where('id', $id)->get();
        return view('admin.attendance.attendance-list', compact('attendances', 'user'));
    }
}
