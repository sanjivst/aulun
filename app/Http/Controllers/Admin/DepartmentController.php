<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.department.register');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = request()->validate([
        'name' => 'required',
        'details' => 'required',
        ]);

        $department = new Department;
        $department->name = $request->name;
        $department->details = $request->details;
        $department->save();
        return redirect()->route('department.show')->with('success', 'Department Added Successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $departments = Department::orderBy('updated_at', 'desc')->get();
        return view('admin.department.department-list', compact('departments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('admin.department.edit', compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
      $data = request()->validate([
      'name' => 'required',
      'details' => 'required',
      ]);

        $department->name = $request->name;
        $department->details = $request->details;
        $department->save();
        return redirect()->back()->with('success', 'Department Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(Department $department)
    {
        $department->delete();
        return redirect()->back()->with('success', 'Department Deleted Successfully!!');
    }

    public function getEmployee($department_id){
        $user = User::where('department_id', $department_id)->get();
        return $user;
    }

}
