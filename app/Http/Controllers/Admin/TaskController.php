<?php

namespace App\Http\Controllers\Admin;
use App\Task;
use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // $tasks = Task::orderBy('updated_at', 'desc')->get();
        $departments = Department::all();
        return view('admin.task.register', compact('departments'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $data = request()->validate([
        //   'department_id' => 'required',
        //   'user_id' => 'required|after:department_id',
        //   'priority' => 'required',
        //   'date_and_time' => 'required',
        //   'title' => 'required',
        //   'description' => 'required'
        //
        // ]);
        
        $task = new Task;
        $task->user_id = $request->user_id;
        $task->priority = $request->priority;
        $task->date_and_time = $request->date_and_time;
        $task->title = $request->title;
        $task->description = $request->description;

        $task->save();
        return redirect()->back()->with('success','Task Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        return view('admin.task.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        // $task->user->full_name = $request->user->full_name;
        // $data = request()->validate([
        // // 'department_id' => 'required',
        // 'user_id' => 'required',
        // 'priority' => 'required',
        // 'date_and_time' => 'required',
        // 'title' => 'required',
        // 'description' => 'required'
        // ]);

      // return $task;

        $task->save();
        $task->date_and_time = $request->date_and_time;
        $task->description = $request->description;
        $task->priority = $request->priority;
        $task->title = $request->title;
        $task->save();
        return redirect()->back()->with('success', 'Task Updated Successfully!!!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return redirect()->back()->with('success', 'Task Deleted Successfully!!!');

    }
}
