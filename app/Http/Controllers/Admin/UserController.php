<?php

namespace App\Http\Controllers\Admin;

use App\Department;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::orderBy('updated_at', 'desc')->get();
        $departments = Department::all();
        return view('admin.user.register', compact('departments', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
      $userId = isset($user) ? $user->id : null;


        $rules = [
            'staff_id' => 'required|min:3|unique:users',
            'full_name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'gender' => 'required',
            'contact_one' => 'required|min:10|max:10',
            'contact_two' => 'required|min:10|max:10',
            'temp_address' => 'required',
            'street' => 'required',
            'district' => 'required',
            'province' => 'required',
            'post_code' => 'required',
            'country' => 'required',
            'department_id' => 'required',
            'position' => 'required',
            'join_date' => 'required|date',
            'salary' => 'required',
//            'pan_number' => 'required',
            'bank_name' => 'required',
//            'bank_account_number' => 'required',
            'responsibility' => 'required',
        ];

        $this->validate($request, $rules);
//        return $request;


        $user = new User;
        $user->staff_id = $request->staff_id;
        $user->full_name = $request->full_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->contact_one = $request->contact_one;
        $user->contact_two = $request->contact_two;
        $user->guardian_name = $request->guardian_name;
        $user->citizenship_number = $request->citizenship_number;
        $user->temp_address = $request->temp_address;
        $user->street = $request->street;
        $user->district = $request->district;
        $user->province = $request->province;
        $user->post_code = $request->post_code;
        $user->country = $request->country;
        $user->department_id = $request->department_id;
        $user->position = $request->position;
        $user->join_date = $request->join_date;
        $user->salary = $request->salary;
        $user->pan_number = $request->pan_number;
        $user->bank_name = $request->bank_name;
        $user->bank_account_number = $request->bank_account_number;
        $user->responsibility = $request->responsibility;
        $user->save();
//        dd($user);
        return redirect()->route('user.show')->with('success', 'User Created Successfully!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $users = User::orderBy('updated_at', 'desc')->get();
        return view('admin.user.user-list', compact('users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
//
//
//        $user = User::orderBy('updated_at', 'desc')->get();
        $departments = Department::all();
        return view('admin.user.edit', compact('user','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
      $rules = [
          'staff_id' => 'required|min:3',
          'full_name' => 'required|min:3',
          'email' => 'required|email',
          'password' => 'required',
          'gender' => 'required',
          'contact_one' => 'required|min:10|max:10',
          'contact_two' => 'min:10|max:10',
          'temp_address' => 'required',
          'street' => 'required',
          'district' => 'required',
          'province' => 'required',
          'post_code' => 'required',
          'country' => 'required',
          'department_id' => 'required',
          'position' => 'required',
          'join_date' => 'required|date',
          'salary' => 'required',
//            'pan_number' => 'required',
          'bank_name' => 'required',
//            'bank_account_number' => 'required',
          'responsibility' => 'required',
      ];
        $this->validate($request, $rules);

        $user->staff_id = $request->staff_id;
        $user->full_name = $request->full_name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->gender = $request->gender;
        $user->contact_one = $request->contact_one;
        $user->contact_two = $request->contact_two;
        $user->guardian_name = $request->guardian_name;
        $user->citizenship_number = $request->citizenship_number;
        $user->temp_address = $request->temp_address;
        $user->street = $request->street;
        $user->district = $request->district;
        $user->province = $request->province;
        $user->post_code = $request->post_code;
        $user->country = $request->country;
        $user->department_id = $request->department_id;
        $user->position = $request->position;
        $user->join_date = $request->join_date;
        $user->salary = $request->salary;
        $user->pan_number = $request->pan_number;
        $user->bank_name = $request->bank_name;
        $user->bank_account_number = $request->bank_account_number;
        $user->responsibility = $request->responsibility;

        $user->save();
        return redirect()->back()->with('success', 'User Data Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->back()->with('success', 'User Deleted Successfully!!');
    }
}
