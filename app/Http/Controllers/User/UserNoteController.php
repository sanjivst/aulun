<?php

namespace App\Http\Controllers\User;
use App\UserNote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;

class UserNoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usernote = new UserNote;
        $usernote->user_id = Auth::user()->id;
        $usernote->description = $request->description;
        $usernote->save();
        return redirect()->route('user.dashboard')->with('success', 'Users Note Stored Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserNote $usernote)
    {
        return view('user.note.edit',compact('usernote'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserNote $usernote, Request $request)
    {
        $usernote->description = $request->description;
        $usernote->save();
        return redirect()->route('user.dashboard')->with('success', 'User Note Updated Successfully!!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserNote $usernote)
    {
        $usernote -> delete();
        return redirect()->route('user.dashboard')->with('success', 'User Note Deleted Successfully');
  
    }
}
