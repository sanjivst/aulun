<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveApplication extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
