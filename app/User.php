<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'address', 'contact_num', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function department()
    {
        return $this->belongsTo('App\Department', 'department_id');
    }

    public function task()
    {
        return $this->hasMany('App\Task');
    }


    public function leaveApplication()
    {
        return $this->hasMany('App\LeaveApplication');
    }

    public function admin()
    {
        return $this->belongsTo('App\Attendance');
    }

    public function agreement()
    {
        return $this->hasMany('App\Agreement');
    }
}
