<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('staff_id');
            $table->string('full_name');
            $table->string('email');
            $table->timestamp('email_verified_at');
            $table->string('password');
            $table->string('gender');
            $table->string('contact_one');
            $table->string('contact_two');
            $table->string('guardian_name');
            $table->string('citizenship_number');
            $table->string('temp_address');
            $table->string('street');
            $table->string('district');
            $table->string('province');
            $table->string('post_code');
            $table->string('country');
            $table->string('position');
            $table->string('join_date');
            $table->string('salary');
            $table->string('pan_number');
            $table->string('bank_name');
            $table->string('bank_account_number');
            $table->text('responsibility');
            $table->unsignedBigInteger('department_id');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('department_id')->references('id')->on('departments')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
