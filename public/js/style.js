$(document).ready(function(){

	$('#myTable').DataTable();
	$('#myTable1').DataTable();

	jQuery('#datepicker-autoclose').datepicker({
	    autoclose: true,
	    todayHighlight: true
	});

	$('.summernote').summernote({
	    height: 200, // set editor height
	    minHeight: null, // set minimum height of editor
	    maxHeight: null, // set maximum height of editor
	    focus: false // set focus to editable area after initializing summernote
	});
	$('.summerNotes').summernote({
	    
	});

	 $('.datetime').daterangepicker({
        timePicker: true,
        timePickerIncrement: 30,
        locale: {
            format: 'MM/DD/YYYY h:mm A'
        }
    });

	 $(".select2").select2();

	 
});