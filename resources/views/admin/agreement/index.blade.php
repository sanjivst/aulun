@extends('admin.layout.layout')
@section('content')
    <div class="row page-titles">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">User List for Agreements</h4>
                    <hr>
                    <div class="table-responsive">
                        <table id="myTable" class="table table-bordered">
                            <thead class="tbl-th-title">
                            <tr>
                                <th>S.N</th>
                                <th>Name</th>
                                <th>Department</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php($i = 1)
                            @foreach($users as $user)
                                <tr>
                                    <td align="center">{{ $i++ }}</td>
                                    <td align="center">{{ $user->full_name }}</td>
                                    <td align="center">{{ $user->department->name }}</td>
                                    <td align="center">
                                        <a href = "{{ route('agreement.create', $user->id) }}" class = "btn btn-primary btn-sm">Add</a>
                                        <a href = "{{ route('agreement.show', $user->id) }}" class = "btn btn-success btn-sm">Show</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
