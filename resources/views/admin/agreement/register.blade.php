@extends('admin.layout.layout')
@section('content')
    <div class="row page-titles">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add Agreement for {{ $user->full_name }}</h4>
                    <hr>
                    {{Form::open(['method'=>'post', 'route'=>['agreement.store', $user->id], 'enctype' => 'multipart/form-data'])}}
                        <div class="form-group">
                            <label for="agreement" class="control-label col-sm-3">Upload File</label>
                            <div class="col-sm-9">
                                <input class="form-control" type="file" name="agreement" id="agreement">
                            </div>
                        </div>
                    <div class="form-actions m-t-20">
                        <button type="submit" class="btn btn-success"> Submit </button>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@endsection
