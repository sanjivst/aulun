@extends('admin.layout.layout')
@section('content')
{{Form::open(['method'=>'post', 'route'=>'announcement.store', 'enctype' => 'multipart/form-data'])}}
        <div class="row page-titles">
            <div class="col-12">
                <div class="card card-outline-info">
                    <div class="card-body">
                        <h4 class="card-title">Add Announcement</h4>
                        <hr>
                        <!-- <form action="#"> -->
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for = "name">Department</label>
                                        <select name = "department_name" id = "department_name" type = "string" class="select2 form-control custom-select" Multiple style="width: 100%">
                                            <option>--Select Department--</option>
                                            @foreach($departments as $department)
                                                <option value = "{{$department->name}}">{{$department->name}}</option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label>Employee Name</label>
                                    <select name = "user_name" type = "string"class="select2 form-control custom-select" Multiple style="width: 100%">
                                        <option>--Select Employee Name--</option>
                                        <option>All</option>
                                        <option>Gopal Basnet</option>
                                        <option>Shramik Nakarmi</option>
                                        <option>ADMIN Department</option>
                                        <option>MARKETING Department</option>
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Title</label>
                                        <input name = "title" type = "string" type="text" id="title" class="form-control" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="control-label">Description</label>
                                    <textarea name = "description" class="form-control" placeholder=""></textarea>
                                    <!-- <div class="summernote">
                                    </div> -->
                                </div>
                            </div>
                        </div>

                        <div class="form-actions m-t-20">
                            <button type="submit" class="btn btn-success"> Submit </button>
                        </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>
{{Form::close()}}

@endsection
