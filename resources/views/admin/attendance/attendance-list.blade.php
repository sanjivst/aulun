@extends('admin.layout.layout')
@section('content')
<div class="row page-titles">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">List of Attendance</h4>
                        <hr>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered">
                                <thead class="tbl-th-title">
                                <tr>
                                    <th>S.N</th>
                                    <th>Date</th>
                                    <th>Entry Time</th>
                                    <th>Exit Time</th>
                                    <th>Working Time</th>
                                </tr>
                                </thead>
                                <tbody class="tbl-th-info">
                                @php($i = 1)
                                @foreach($attendances as $attendance)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $attendance->date }}</td>
                                        <td>{{$attendance->entry_time }}</td>
                                        <td>{{ $attendance->exit_time }}</td>
                                        <td>{{$attendance->working_time}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection