@extends('admin.layout.layout')
@section('content')
{{Form::open(['method'=>'post', 'route'=>['attendance.store', $id], 'enctype' => 'multipart/form-data'])}}
        <div class="row page-titles">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Attendance</h4>
                        <hr>
                        <form class="form-horizontal">
                        <div class="form-group">
                                <label>Date</label>
                                <input type="date" name = "date"  class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Entry Time</label>
                                <input type="time" name = "entry_time"  class="form-control" required>
                            </div>

                            <div class="form-group">
                                <label>Exit Time</label>
                                <input type="time" class="form-control " name = "exit_time" required>
                            </div>

                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    {{Form::close()}}

@endsection