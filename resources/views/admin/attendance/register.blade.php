@extends('admin.layout.layout')
@section('content')

<div class="row page-titles">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">List of User</h4>
                        <hr>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered">
                                <thead class="tbl-th-title">
                                <tr>
                                    <th>S.N</th>
                                    <th>Name</th>
                                    <th>Department</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody class="tbl-th-info">
                                @php($i = 1)
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $user->full_name  }}</td>
                                        <td>{{ $user->department->name }}</td>
                                        <td>
                                        <a class="btn btn-success" href="{{route('attendance.fillForm', $user->id)}}" role="button">Add Attendance</a>  
                                        <a class="btn btn-success" href="{{route('attendance.list', $user->id)}}" role="button">Show Attendance</a>  
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


@endsection