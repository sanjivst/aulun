@extends('admin.layout.layout')
@section('content')
    <div class="row page-titles">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">List of Departments</h4>
                        <hr>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered">
                                <thead class="tbl-th-title">
                                <tr>
                                    <th>S.N</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody class="tbl-th-info">
                                @php($i = 1)
                                @foreach($departments as $department)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $department->name  }}</td>
                                        <td>
                                            {{ $department->details }}
                                        </td>
                                        <td class="table-btn-circle">
                                            <a href = "{{route('department.edit',$department->id)}}" class = "btn btn-success btn-sm">Edit</a>
                                            <a href = "{{route('department.destroy',$department->id)}}" class = "btn btn-danger btn-sm">Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
