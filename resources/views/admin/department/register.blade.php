@extends('admin.layout.layout')
@section('content')
    {{Form::open(['method'=>'post', 'route'=>'department.store', 'enctype' => 'multipart/form-data'])}}
        <div class="row page-titles">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Add Department</h4>
                        <hr>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label>Department Name</label>
                                <input type="string" name = "name" value="{{ old('name') }}" class="form-control @error('name') is-invalid @enderror" required>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>Department Details</label>
                                <textarea class="form-control @error('details') is-invalid @enderror" name = "details" value="{{ old('details') }}" rows="5" required></textarea>
                                @error('details')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    {{Form::close()}}

@endsection
