@extends('admin.layout.layout')
@section('content')

    <div id="app">
        <br>
        <example-component></example-component>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>

@endsection
