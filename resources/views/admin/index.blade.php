@extends('admin.layout.layout')
@section('content')
   <div class="row page-titles">
		        <div class="col-12">
		            <div class="card">
                  <div class="card-body">
		                    <h4 class="card-title h4-list-specific">List of Specific Task</h4>
                            <a href="{{ route('task.create') }}" class="list-specific-create">Add Specific Task</a>
                            <hr>
                            <div class="table-responsive">
                                <table id="myTable1" class="table table-bordered">
                                    <thead class="tbl-th-title">
                                        <tr>
                                            <th>S.N</th>
                                            <th>Department</th>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Date</th>
                                            <th>Description</th>
                                            <th>Priority</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbl-th-info">
                                        @php($i = 1)
                                        @foreach($tasks as $task)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $task->user->department->name }}</td>
                                            <td>{{ $task->id }}</td>
                                            <td>{{ $task->user->full_name }}</td>
                                            <td>{{ $task->date_and_time }}</td>

                                            <!-- Button trigger modal -->
                                            <td>
                                                <button type="button" class="btn btn btn sm" data-toggle="modal" data-target="#exampleModalLong">
                                                  Description
                                                </button>
                                            </td>
                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLongTitle">Task Description</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {!! html_entity_decode($task->description)!!}
                                                    </div>

                                                    </div>
                                                </div>
                                                </div>



                                            <!-- Priority -->
                                                @if( $task->priority == 'high')
                                                <td><span class="badge badge-danger badge-size">High</span></td>
                                                @elseif( $task->priority == 'medium')
                                                <td><span class="badge badge-primary badge-size">Medium</span></td>
                                                @else( $task->priority == 'low')
                                                <td><span class="badge badge-success badge-size">Low</span></td>
                                                @endif
                                            <!-- Status -->
                                                @if( $task->status == 0)
                                                <td>Pending</td>
                                                @elseif($task->status == 1)
                                                   <td>Processing</td>
                                                @else
                                                    <td>Completed</td>
                                                @endif
                                            <td class="table-btn-circle">
                                                <a href = "{{route('task.edit', $task->id)}}" class = "btn btn-success btn-sm">Edit</a>
                                                <a href = "{{route('task.destroy', $task->id)}}" class = "btn btn-danger btn-sm">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
		                </div>
		            </div>


                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Write Notes</h4>
                            <hr>
                            {{Form::open(['method' => 'post', 'route' => 'note.store', 'enctype' => 'multipart/form-data'])}}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="">Description</label>
                                            <textarea name = "description" class="form-control">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions m-t-20">
                                    <button type="submit" class="btn btn-info"> Submit </button>
                                </div>
                            {{Form::close()}}
  
                            <hr>

                            <h4 class="card-title">List Of Notes</h4>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered">
                                    <thead class="tbl-th-title">
                                        <tr>
                                            <th>S.N</th>
                                            <th>Date & Time</th>
                                            <th>Notes</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbl-th-info">
                                        @php($i = 1)
                                        @foreach($notes as $note)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $note->created_at }}</td>
                                            <td>{{$note->description}}</td>
                                            <td class="table-btn-circle">
                                                <a href = "{{route('note.edit', $note->id)}}" class = "btn btn-success btn-sm">Edit</a>
                                                <a href = "{{route('note.destroy', $note->id)}}" class = "btn btn-danger btn-sm">Delete</a>

                                            </td>
                                        </tr>
                                        @endforeach

                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>

		        </div>
            </div>
            <div class="card">
                        <div class="card-body">
                            <nav class="sidebar-nav" align="center" >
                                <ul id="sidebarnav">
                                    <li>
                                        
                                        <a href="{{route('attendance.userList')}}" aria-expanded="false">
                                            <i class="mdi mdi-bullseye"></i>
                                            <span class="hide-menu">Attendance </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="has-arrow " href="{{route('leaveApplication.list')}}" aria-expanded="false">
                                            <i class="mdi mdi-bullseye"></i>
                                            <span class="hide-menu">See Leave Application</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="has-arrow " href="{{ route('events.page') }}" aria-expanded="false">
                                            <i class="mdi mdi-bullseye"></i>
                                            <span class="hide-menu">Upload Holidays</span>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
@endsection
