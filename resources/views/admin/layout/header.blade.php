<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon.png')}}">
    <title>Heaven Maker</title>

    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/dataTables.bootstrap4.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset('css/responsive.dataTables.min.css')}}" rel="stylesheet" type="text/css" >
    <link href="{{asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/summernote.css')}}" rel="stylesheet" />
    <link href="{{asset('css/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/daterangepicker.css')}}" rel="stylesheet">
    <link href="{{asset('css/steps.css')}}" rel="stylesheet">
    <link href="{{asset('css/select2.min.css')}}" rel="stylesheet">
    <link href="{{asset('css/style.css')}}" rel="stylesheet">
    <link href="{{asset('css/colors/blue.css')}}" id="theme" rel="stylesheet">
{{--    <link rel="stylesheet" href="{{ asset('css/app.css') }}">--}}


</head>

<body class="fix-header card-no-border logo-center">

    <div id="main-wrapper">
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-light">
                <div class="navbar-header header-logo-div">
                    <a class="navbar-brand header-logo-a" href="{{ route('admin.dashboard') }}">
                        <img src="{{asset('images/logo-01.png')}}" alt="homepage" class="img-fluid" />
                    </a>
                </div>
                <div class="navbar-collapse">
                    <ul class="navbar-nav mr-auto mt-md-0">
                        <li class="nav-item">
                            <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)">
                                <i class="mdi mdi-menu"></i>
                            </a>
                        </li>
                        <li class="nav-item hidden-sm-down search-box">
                            <a class="nav-link hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)">
                                <i class="fa fa-search"></i>
                            </a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search & enter"> <a class="srh-btn"><i class="fa fa-times"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
{{--                        <li class="nav-item dropdown">--}}
{{--                            <a class="nav-link dropdown-toggle text-muted text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
{{--                                <i class="mdi mdi-bell"></i>--}}
{{--                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>--}}
{{--                            </a>--}}
{{--                            <div class="dropdown-menu dropdown-menu-right mailbox scale-up">--}}
{{--                                <ul>--}}
{{--                                    <li>--}}
{{--                                        <div class="drop-title">Notifications</div>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <div class="message-center">--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>--}}
{{--                                                <div class="mail-contnet">--}}
{{--                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>--}}
{{--                                            </a>--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>--}}
{{--                                                <div class="mail-contnet">--}}
{{--                                                    <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span> </div>--}}
{{--                                            </a>--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>--}}
{{--                                                <div class="mail-contnet">--}}
{{--                                                    <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span> </div>--}}
{{--                                            </a>--}}
{{--                                            <a href="#">--}}
{{--                                                <div class="btn btn-primary btn-circle">--}}
{{--                                                    <i class="ti-user"></i>--}}
{{--                                                </div>--}}
{{--                                                <div class="mail-contnet">--}}
{{--                                                    <h5>Pavan kumar</h5>--}}
{{--                                                    <span class="mail-desc">Just see the my admin!</span>--}}
{{--                                                    <span class="time">9:02 AM</span>--}}
{{--                                                </div>--}}
{{--                                            </a>--}}
{{--                                        </div>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a class="nav-link text-center" href="notification.php">--}}
{{--                                            <strong>Check all notifications</strong>--}}
{{--                                            <i class="fa fa-angle-right"></i>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </li>--}}
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="{{asset('images/admin.jpg')}}" alt="user" class="profile-pic" /></a>
                            <div class="dropdown-menu dropdown-menu-right scale-up">
                                <ul class="dropdown-user">

{{--                                    <li role="separator" class="divider"></li>--}}
                                    <li><a href="{{ route('profile') }}"><i class="ti-user"></i> My Profile</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                        </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <aside class="left-sidebar">
            <div class="scroll-sidebar">
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li>
                            <a href="{{ route('admin.dashboard') }}" aria-expanded="false">
                                <i class="mdi mdi-gauge"></i>
                                <span class="hide-menu">Dashboard </span>
                            </a>
                        </li>
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">User</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('user.create') }}">Add User</a></li>
                                <li><a href="{{ route('user.show') }}">User List</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">Department</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{ route('department.create') }}">Add Department</a></li>
                                <li><a href="{{ route('department.show') }}">Department List</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">Task</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('task.create')}}">Add Specific Task</a></li>
                            </ul>
                        </li>
{{--                        <li>--}}
{{--                            <a class="has-arrow " href="#" aria-expanded="false">--}}
{{--                                <i class="mdi mdi-bullseye"></i>--}}
{{--                                <span class="hide-menu">Announcement</span>--}}
{{--                            </a>--}}
{{--                            <ul aria-expanded="false" class="collapse">--}}
{{--                                <li><a href="{{ route('announcement.create') }}">Add Announcement</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
                        <li>
                            <a class="has-arrow " href="#" aria-expanded="false">
                                <i class="mdi mdi-bullseye"></i>
                                <span class="hide-menu">Agreements</span>
                            </a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="{{route('agreement.index')}}">User Agreements</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </aside>




