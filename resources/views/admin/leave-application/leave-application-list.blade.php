@extends('admin.layout.layout')
@section('content')
    <div class="row page-titles">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">List of Leave Application</h4>
                        <hr>
                        <div class="table-responsive">
                            <table id="myTable" class="table table-bordered">
                                <thead class="tbl-th-title">
                                <tr>
                                    <th>S.N</th>
                                    <th>Applicant</th>
                                    <th>Department Name</th>
                                    <th>Reason</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Notes</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody class="tbl-th-info">
                                @php($i = 1)
                                @foreach($leaves as $leave)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{$leave->user->full_name}}</td>
                                        <td>{{$leave->user->department->name}}</td>
                                        <td>{{ $leave->reason }}</td>
                                        <td>{{ $leave->startdate }}</td>
                                        <td>{{ $leave->enddate }}</td>
                                        <td>{{ $leave->note }}</td>
                                        <td>
                                            {{Form::open(['method' => 'post', 'route' => ['submitResponse', $leave->id], 'enctype' => 'multipart/form-data'])}}
                                                <select name="response" class="form-control">
                                                    <option value="ACCEPTED" {{$leave->response == 'ACCEPTED' ? 'selected' : ''}}> Accept</option>
                                                    <option value="REJECTED" {{$leave->response == 'REJECTED' ? 'selected' : ''}}> Reject</option>
                                                </select>
                                        </td>
                                        <td>
                                            <input type="submit" class="btn btn-info">  
                                                
                                        {{Form::close()}}

                                        </td> 
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
