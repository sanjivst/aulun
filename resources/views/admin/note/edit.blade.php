@extends('admin.layout.layout')
@section('content')
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Note</h4>
                            <hr>
                            {{Form::open(['method' => 'patch', 'route' => ['note.update', $note->id], 'enctype' => 'multipart/form-data'])}}
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="">Description</label>
                                            <textarea name = "description" class="form-control">{{$note->description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions m-t-20">
                                    <button type="submit" class="btn btn-success"> Submit </button>
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>

@endsection
