@extends('admin.layout.layout')
@section('content')
		    <div class="row p-t-20">
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-body p-t-0 p-b-0">
                            <center class="m-t-20">
                                <img src="{{asset('images/admin.jpg')}}" class="img-circle" width="150" />
                                <h4 class="card-title m-t-10">{{ Auth::user()->name }}</h4>
                                <h6 class="card-subtitle">Super Admin</h6>
                            </center>
                        </div>
                        <hr>
                        <div class="card-body p-t-0">
                            <small class="text-muted">Email address </small>
                            <h6>{{ Auth::user()->email }}</h6> <small class="text-muted db">Phone</small>
                            <h6>{{ Auth::user()->contact_num }}</h6> <small class="text-muted db">Address</small>
                            <h6>{{ Auth::user()->address }}</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Edit Profile</h4>
                            <hr>
                            {{Form::open(['method'=>'patch', 'route'=>['profile.update',$admin->id], 'enctype' => 'multipart/form-data'])}}
                                <div class="form-body">
                                    <div class="row p-t-20">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Full Name</label>
                                                <input type="text" name="name" id="fullName" value="{{ old('name') ?? $admin->name }}" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Address</label>
                                                <input type="text" name="address" id="adminAddress" value="{{ old('address') ?? $admin->address }}" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Email Address</label>
                                                <input type="email" name="email" id="adminEmail" value="{{ old('email') ?? $admin->email }}" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Contact Number </label>
                                                <input type="number" name="contact_num" id="adminNumber-1" value="{{ old('contact_num') ?? $admin->contact_num }}" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Change Password</label>
                                                <input type="password" name="password" id="password" value="{{ old('password') ?? $admin->password }}" class="form-control" placeholder="">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-actions m-t-20">
                                    <button type="submit" class="btn btn-success"> Submit </button>
                                </div>
                                {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
@endsection
