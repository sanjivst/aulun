@extends('admin.layout.layout')
@section('content')
    {{Form::open(['method'=>'patch', 'route'=>['task.update', $task->id], 'enctype' => 'multipart/form-data'])}}
		    <div class="row page-titles">
		        <div class="col-12">
		            <div class="card card-outline-info">
                        <div class="card-body">
                        	<h4 class="card-title">Edit Specific Task</h4>
                        	<hr>
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for = "department_id">Department</label>
                                                <input type="string" name = "name" class="form-control" value="{{$task->user->department->name}}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Employee Name</label>
                                                <input type="string" name = "full_name" class="form-control" value="{{$task->user->full_name}}" disabled>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <div class="form-group">
                                                <label>Priority</label>
                                                <select name = "priority" type = "string" id ="prioritySelect" required class="form-control">
                                                    <option value="">--Select Priority--</option>
                                                    <option value="high" {{ $task->priority == 'high' ? 'selected' : '' }}>High</option>
                                                    <option value="medium" {{ $task->priority == 'medium' ? 'selected' : '' }}>Medium</option>
                                                    <option value="low" {{ $task->priority == 'low' ? 'selected' : '' }}>Low</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <label  for="">Date & Time</label>
                                            <div class='input-group mb-3'>
                                                <input name = "date_and_time" value="{{$task->date_and_time}}" type = "string" required class="form-control datetime" />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <input name = "title" value="{{ old('title') ?? $task->title }}" type= "string" id="title2" class="form-control" required>
                                            </div>
                                        </div>
                                    </div>
                                    

                                <div class="row">
                                    <div class="col-md-12{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label for="">Description</label>
                                        <textarea type="text" name="description" required class="form-control summernote">{!!html_entity_decode($task->description)!!}</textarea>
                                        <small class="text-danger">{{ $errors->first('description') }}</small>
                                    </div>
                                </div>


                                <div class="form-actions m-t-20">
                                    <button type="submit" class="btn btn-success"> Submit </button>
                                </div>
                        </div>
		            </div>
		        </div>
		    </div>
		</div>

    {{Form::close()}}

@endsection
