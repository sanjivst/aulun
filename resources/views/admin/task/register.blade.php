@extends('admin.layout.layout')
@section('content')
    {{Form::open(['method'=>'post', 'route'=>'task.store', 'enctype' => 'multipart/form-data'])}}
		    <div class="row page-titles">
		        <div class="col-12">
		            <div class="card card-outline-info">
                        <div class="card-body">
                        	<h4 class="card-title">Add Specific Task</h4>
                        	<hr>
                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for = "department_id">Department</label>
                                                <select name = "department_id" id = "mySelect" type = "string" class="form-control @error('department_id') is-invalid @enderror" required>
                                                    <option value = "">--Select Department--</option>
                                                    @foreach($departments as $department)
                                                        <option value = "{{$department->id}}">{{$department->name}}</option>
                                                    @endforeach
                                                    @error('department_id')
                                                      <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Employee Name</label>
                                                <select name = "user_id" id="myEmployeeList" value = "{{old('user_id')}}" type = "string" class="form-control @error('user_id') is-invalid @enderror" required>
                                                  @error('user_id')
                                                    <span class="invalid-feedback" role="alert">
                                                      <strong>{{ $message }}</strong>
                                                  </span>
                                                  @enderror
                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-md-6'>
                                            <div class="form-group">
                                                <label>Priority</label>
                                                <select name = "priority" value = "{{old('priority')}}" type = "string" id ="prioritySelect" class="form-control @error('priority') is-invalid @enderror" required>
                                                    <option value="">--Select Priority--</option>
                                                    <option value="high" {{ old('priority') }}>High</option>
                                                    <option value="medium" {{ old('priority') }}>Medium</option>
                                                    <option value="low" {{ old('priority') }}>Low</option>
                                                    @error('priority')
                                                      <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                                </select>
                                            </div>
                                        </div>
                                        <div class='col-md-6'>
                                            <label  for="">Date & Time</label>
                                            <div class='input-group mb-3'>
                                                <input name = "date_and_time" type = "string" value = "{{old('date_and_time')}}" class="form-control datetime @error('date_and_time') is-invalid @enderror" required />
                                                <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label">Title</label>
                                                <input name = "title" type= "string" value = "{{old('title')}}" id="title2" class="form-control @error('title') is-invalid @enderror" placeholder="" required>
                                                @error('title')
                                                  <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                        </div>
                                    </div>
                                    
                                    
                                    <div class="row">
                                    <div class="col-md-12{{ $errors->has('description') ? ' has-error' : '' }}">
                                        <label for="">Add Description</label>
                                        <textarea type="text" name="description" required class="form-control summernote">{{ old('description') }}</textarea>
                                        <small class="text-danger">{{ $errors->first('description') }}</small>
                                    </div>
                                </div>


                                <div class="form-actions m-t-20">
                                    <button type="submit" class="btn btn-success"> Submit </button>
                                </div>
                        </div>
		            </div>
		        </div>
		    </div>
		</div>

    {{Form::close()}}
    <script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous" ></script>
    <script type="text/javascript">

        $('#mySelect').change(function()
            {
                $('#myEmployeeList').empty();
                var id = "";
                $( "#mySelect option:selected " ).each(function() {
                id += $( this ).val() + " ";
                getUserByajax(id);
                });
            });
        function getUserByajax(id){
            $.ajax({url: '/admin/getEmployee/'+id, success: function(data){

                $('#myEmployeeList').append("<option selected disabled > --Select A User -- </option>");
                data.forEach(function (option) {

                    $('#myEmployeeList').append("<option value=" + option['id'] + ">" + option['full_name'] + "</option>");
                });
            }

            });
        }
</script>




@endsection
