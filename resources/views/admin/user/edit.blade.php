@extends('admin.layout.layout')
@section('content')
    {{Form::open(['method'=>'patch', 'route'=>['user.update',$user->id], 'enctype' => 'multipart/form-data'])}}
        <div class="row page-titles">
            <div class="col-12">
                <div class="card card-outline-info">
                    <div class="card-body wizard-content">
                        <h4 class="card-title">Edit User</h4>
                        <form action="#" class="tab-wizard wizard-circle">
                        <!-- Step 1 -->
                        <h6>Personal Info</h6>
                        <section>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('staff_id') ? ' has-error' : '' }}">
                                        <label class="control-label">Staff ID</label>
                                        <input type="text" id="staffId" value="{{ old('staff_id') ?? $user->staff_id }}" name="staff_id" class="form-control" placeholder="">
                                        <small class="text-danger">{{ $errors->first('staff_id') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                                        <label class="control-label">Full Name</label>
                                        <input type="text" id="fullName" value="{{ old('full_name') ?? $user->full_name }}" name="full_name" class="form-control" placeholder="">
                                        <small class="text-danger">{{ $errors->first('full_name') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label class="control-label">Email Address</label>
                                        <input type="email" id="email" name="email" value="{{ old('email') ?? $user->email }}" class="form-control" placeholder="">
                                        <small class="text-danger">{{ $errors->first('email') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Password</label>
                                        <input type="password" id="password" name="password" value="{{ old('password') ?? $user->password }}" class="form-control" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label class="control-label">Gender</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="custom-radio">
                                            <input id="radio1" name="gender" value="male" type="radio" class="custom-control-input" {{ $user->gender == 'male' ? 'checked' : '' }}>
                                            <span class="custom-control-label">Male</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="custom-radio">
                                            <input id="radio2" name="gender" value="female" type="radio" class="custom-control-input" {{ $user->gender == 'female' ? 'checked' : '' }}>
                                            <span class="custom-control-label">Female</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="custom-radio">
                                            <input id="radio1" name="gender" value="other" type="radio" class="custom-control-input" {{ $user->gender == 'female' ? 'checked' : '' }}>
                                            <span class="custom-control-label">Other</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('contact_one') ? ' has-error' : '' }}">
                                        <label class="control-label">Contact Number-1 </label>
                                        <input type="number" id="number-1" name="contact_one" value="{{ old('contact_one') ?? $user->contact_one }}" class="form-control" placeholder="">
                                        <small class="text-danger">{{ $errors->first('contact_one') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('contact_two') ? ' has-error' : '' }}">
                                        <label class="control-label">Contact Number-2 </label>
                                        <input type="number" id="number-2" name="contact_two" value="{{ old('contact_two') ?? $user->contact_two }}" class="form-control" placeholder="">
                                        <small class="text-danger">{{ $errors->first('contact_two') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('guardian_name') ? ' has-error' : '' }}">
                                        <label class="control-label">Guardians Name</label>
                                        <input type="text" id="gaurdainName" name="guardian_name"  value="{{ old('guardian_name') ?? $user->guardian_name }}" class="form-control" placeholder="">
                                        <small class="text-danger">{{ $errors->first('guardian_name') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Citizenship Number</label>
                                        <input type="text" name="citizenship_number" value="{{ old('citizenship_number') ?? $user->citizenship_number }}" class="form-control">
                                    </div>
                                </div>
                            </div>
{{--                            <div class="row">--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <label class="control-label">User Image</label>--}}

{{--                                    <div class="form-group">--}}
{{--                                        @if ("/images/users/profiles/{{ $user->user_image }}")--}}
{{--                                            <img src="{{ asset('images/users/profiles/' . $user->user_image) }}" width="50px;" height="50px;">--}}
{{--                                        @else--}}
{{--                                            <p>No image found</p>--}}
{{--                                        @endif--}}
{{--                                        image <input type="file" name="user_image" value="{{ $user->ff }}"/>--}}
{{--                                    </div>--}}

{{--                                    --}}{{--                                    <div class="custom-file mb-3">--}}
{{--                                    --}}{{--                                        <input type="file" name="user_image"  value="{{ old('user_image') ?? $employee->user_image }}" class="custom-file-input" id="customFile">--}}
{{--                                    --}}{{--                                        <label class="custom-file-label form-control" for="customFile">Choose file</label>--}}
{{--                                    --}}{{--                                        <small class="form-control-feedback"> passport size photo </small>--}}
{{--                                    --}}{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="col-md-6">--}}
{{--                                    <label class="control-label">Upload citizenship document</label>--}}
{{--                                    <div class="form-group">--}}
{{--                                        @if ("/images/users/documents/{{ $employee->citizenship_document }}")--}}
{{--                                            <img src="{{ asset('images/users/documents/' . $employee->citizenship_document) }}" width="50px;" height="50px;">--}}
{{--                                        @else--}}
{{--                                            <p>No image found</p>--}}
{{--                                        @endif--}}
{{--                                        image <input type="file" name="citizenship_document" value="{{ $employee->citizenship_document }}"/>--}}
{{--                                    </div>--}}
{{--                                    --}}{{--                                    <div class="custom-file mb-3">--}}
{{--                                    --}}{{--                                        <input type="file" name="citizenship_document" value="{{ old('citizenship_document') ?? $employee->citizenship_document }}" class="custom-file-input" id="customFile2">--}}
{{--                                    --}}{{--                                        <label class="custom-file-label form-control" for="customFile2">Choose file</label>--}}
{{--                                    --}}{{--                                        <small class="form-control-feedback"> both side in one jpg image </small>--}}
{{--                                    --}}{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                        </section>
                        <!-- Step 2 -->
                        <h6>Address</h6>
                        <section>
                            <hr>
                            <h5>Temporary Address</h5>
                            <div class="row">
                                <div class="col-md-12 ">
                                  <div class="form-group{{ $errors->has('temp_address') ? ' has-error' : '' }}">
                                        <label class="control-label">Address</label>
                                        <input type="text" name="temp_address" value="{{ old('temp_address') ?? $user->temp_address }}" class="form-control" placeholder="naya baneshwor, Kathmandu">
                                        <small class="form-control-feedback"> address name with district. </small>
                                        <small class="text-danger">{{ $errors->first('temp_address') }}</small>
                                    </div>
                                </div>
                            </div>
                            <h5>Permanent Address</h5>
                            <hr>
                            <div class="row">
                                <div class="col-md-12 ">
                                  <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                        <label class="control-label">Street</label>
                                        <input name="street"  value="{{ old('street') ?? $user->street }}" type="text" class="form-control">
                                        <small class="text-danger">{{ $errors->first('street') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                                        <label class="control-label">District</label>
                                        <input name="district" value="{{ old('district') ?? $user->district }}" type="text" class="form-control">
                                        <small class="text-danger">{{ $errors->first('district') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                                        <label>Province</label>
                                        <select name="province" class="form-control">
                                            <option value="">--Select your Province--</option>
                                            <option value="province1" {{ $user->province == 'province1' ? 'selected' : '' }}>Province 1</option>
                                            <option value="province2" {{ $user->province == 'province2' ? 'selected' : '' }}>Province 2</option>
                                            <option value="province3" {{ $user->province == 'province3' ? 'selected' : '' }}>Province 3</option>
                                            <option value="province4" {{ $user->province == 'province4' ? 'selected' : '' }}>Province 4</option>
                                            <option value="province5" {{ $user->province == 'province5' ? 'selected' : '' }}>Province 5</option>
                                            <option value="province6" {{ $user->province == 'province6' ? 'selected' : '' }}>Province 6</option>
                                            <option value="province7" {{ $user->province == 'province7' ? 'selected' : '' }}>Province 7</option>
                                        </select>
                                        <small class="text-danger">{{ $errors->first('province') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('post_code') ? ' has-error' : '' }}">
                                        <label class="control-label">Post Code</label>
                                        <input type="text" name="post_code" value="{{ old('post_code') ?? $user->post_code }}" class="form-control">
                                        <small class="text-danger">{{ $errors->first('post_code') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                        <label>Country</label>
                                        <select name="country" class="form-control">
                                            <option value="">--Select your Country--</option>

                                            <option value="nepal" {{ $user->country == 'nepal' ? 'selected' : '' }}>
                                                Nepal
                                            </option>
                                            <option value="india" {{ $user->country == 'india' ? 'selected' : '' }}>
                                                India
                                            </option>
                                            <option value="srianka" {{ $user->country == 'srilanka' ? 'selected' : '' }}>
                                                Sri Lanka
                                            </option>
                                            <option value="usa" {{ $user->country == 'usa' ? 'selected' : '' }}>
                                                USA
                                            </option>
                                        </select>
                                        <small class="text-danger">{{ $errors->first('country') }}</small>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 3 -->
                        <h6>Official Info</h6>
                        <section>
                            <hr>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
                                        <label>Department</label>
                                        <select name="department_id" id = "department_id" class="form-control">
                                            <option>----select department----</option>
                                            @foreach($departments as $department)
                                                <option value = "{{ $department->id }}" {{ $user->department_id == $department->id ? 'selected' : '' }}>{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                        <small class="text-danger">{{ $errors->first('department_id') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                        <label class="control-label">Position</label>
                                        <input name="position" value="{{ old('position') ?? $user->position }}" type="text" class="form-control">
                                        <small class="text-danger">{{ $errors->first('position') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label class="control-label">Date of Joining</label>
                                    <div class="input-group">
                                        <input type="text" name="join_date" value="{{ old('join_date') ?? $user->join_date }}" class="form-control" id="datepicker-autoclose" placeholder="mm/dd/yyyy">
                                        <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
                                        <label class="control-label">Salary</label>
                                        <input name="salary" value="{{ old('salary') ?? $user->salary }}" type="number" class="form-control">
                                        <small class="form-control-feedback"> per month. </small>
                                        <small class="text-danger">{{ $errors->first('salary') }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Pan Number</label>
                                        <input name="pan_number" value="{{ old('pan_number') ?? $user->pan_number }}" type="string" class="form-control">
                                    </div>
                                </div>
{{--                                <div class="col-md-6">--}}
{{--                                    <label class="control-label">Upload User Contract paper</label>--}}
{{--                                    <div class="form-group">--}}
{{--                                        @if ("/images/users/contracts/{{ $user->contract_paper }}")--}}
{{--                                            <img src="{{ asset('images/users/contracts/' . $employee->contract_paper) }}" width="50px;" height="50px;">--}}
{{--                                        @else--}}
{{--                                            <p>No image found</p>--}}
{{--                                        @endif--}}
{{--                                        image <input type="file" name="contract_paper" value="{{ $employee->contract_paper }}"/>--}}
{{--                                    </div>--}}

{{--                                    --}}{{--                                    <div class="custom-file mb-3">--}}
{{--                                    --}}{{--                                        <input type="file" name="contract_paper" value="{{ old('contract_paper') ?? $employee->contract_paper }}" class="custom-file-input" id="customFile3">--}}
{{--                                    --}}{{--                                        <label class="custom-file-label form-control" for="customFile3">Choose file</label>--}}
{{--                                    --}}{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
                                        <label class="control-label">Bank Name</label>
                                        <input type="string" name="bank_name" value="{{ old('bank_name') ?? $user->bank_name }}" class="form-control">
                                        <small class="text-danger">{{ $errors->first('bank_name') }}</small>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label">Bank Account Number</label>
                                        <input type="string" name="bank_account_number" value="{{ old('bank_account_number') ?? $user->bank_account_number }}" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- Step 4 -->
                        <h6>Responsibility</h6>
                        <section>
                            <hr>
                            <div class="row">
                              <div class="col-md-12{{ $errors->has('responsibility') ? ' has-error' : '' }}">
                                    <label for="">Add Responsibility</label>
                                    <textarea name="responsibility" class="form-control summernote">{!!html_entity_decode($user->responsibility)!!}</textarea>
                                    <small class="text-danger">{{ $errors->first('responsibility') }}</small>
                            </div>
                            </div>
                        </section>
                        <div class="form-actions m-t-20">
                            <button type="submit" class="btn btn-success"> Submit </button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
{{Form::close()}}


@endsection
