@extends('admin.layout.layout')
@section('content')
    {{Form::open(['method'=>'post', 'route'=>'user.store', 'enctype' => 'multipart/form-data'])}}
        <div class="row page-titles">
            <div class="col-12">
                <div class="card card-outline-info">
                    <div class="card-body wizard-content">
                        <h4 class="card-title">Add User</h4>
                        <form action="#" class="tab-wizard wizard-circle">
                            <!-- Step 1 -->
                            <h6>Personal Info</h6>
                            <section>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('staff_id') ? ' has-error' : '' }}">
                                            <label for="staff_id" class="control-label">Staff ID</label>
                                            <input type="text" id="staffId" name="staff_id" value="{{ old('staff_id') }}" class="form-control" autocomplete="staff_id" required>
                                            <small class="text-danger">{{ $errors->first('staff_id') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('full_name') ? ' has-error' : '' }}">
                                            <label class="control-label">Full Name</label>
                                            <input type="text" id="fullName" name="full_name" value="{{ old('full_name') }}" class="form-control" placeholder="" required>
                                            <small class="text-danger">{{ $errors->first('full_name') }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="control-label">Email Address</label>
                                            <input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" placeholder="" required>
                                            <small class="text-danger">{{ $errors->first('email') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Password</label>
                                            <input type="password" id="password" name="password" class="form-control" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label class="control-label">Gender</label>
                                    </div>
                                    <div class="col-md-3">
                                                <div class="form-group">
                                                <label class="custom-radio">
                                                    <input id="radio1" name="gender" value="male" type="radio" class="custom-control-input" checked>
                                                    <span class="custom-control-label">Male</span>
                                                </label>
                                                </div>
                                    </div>
                                    <div class="col-md-3">
                                            <div class="form-group">
                                                <label class="custom-radio">
                                                    <input id="radio2" name="gender" value="female" type="radio" class="custom-control-input">
                                                    <span class="custom-control-label">Female</span>
                                                </label>
                                            </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="custom-radio">
                                                <input id="radio1" name="gender" value="other" type="radio" class="custom-control-input">
                                                <span class="custom-control-label">Other</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('contact_one') ? ' has-error' : '' }}">
                                            <label class="control-label">Contact Number-1 </label>
                                            <input type="number" value="{{ old('contact_one') }}" id="number-1" name="contact_one" class="form-control" placeholder="" required>
                                            <small class="text-danger">{{ $errors->first('contact_one') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('contact_two') ? ' has-error' : '' }}">
                                            <label class="control-label">Contact Number-2 </label>
                                            <input type="number" value="{{ old('contact_two') }}" id="number-2" name="contact_two" class="form-control" placeholder="">
                                            <small class="text-danger">{{ $errors->first('contact_two') }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('guardian_name') ? ' has-error' : '' }}">
                                            <label class="control-label">Guardians Name</label>
                                            <input type="text" id="gaurdainName" name="guardian_name" value="{{ old('guardian_name') }}" class="form-control" placeholder="" required>
                                            <small class="text-danger">{{ $errors->first('guardian_name') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Citizenship Number</label>
                                            <input type="text" name="citizenship_number" value="{{ old('citizenship_number') }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
{{--                                <div class="row">--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <label class="control-label">Upload User Image</label>--}}
{{--                                        <div class="custom-file mb-3">--}}
{{--                                            <input type="file" name="user_image">--}}
{{--                                            <label class="custom-file-label form-control" for="customFile">Choose file</label>--}}
{{--                                            <small class="form-control-feedback"> passport size photo </small>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-md-6">--}}
{{--                                        <label class="control-label">Upload citizenship document</label>--}}
{{--                                        <div class="custom-file mb-3">--}}
{{--                                            <input type="file" name="citizenship_document" class="custom-file-input" id="customFile2">--}}
{{--                                            <label class="custom-file-label form-control" for="customFile2">Choose file</label>--}}
{{--                                            <small class="form-control-feedback"> both side in one jpg image </small>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </section>
                            <!-- Step 2 -->
                            <h6>Address</h6>
                            <section>
                                <hr>
                                <h5>Temporary Address</h5>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group{{ $errors->has('temp_address') ? ' has-error' : '' }}">
                                            <label class="control-label">Address</label>
                                            <input type="text" name="temp_address" value="{{ old('temp_address') }}" class="form-control" required placeholder="naya baneshwor, Kathmandu">
                                            <small class="form-control-feedback"> address name with district. </small>
                                            <small class="text-danger">{{ $errors->first('temp_address') }}</small>
                                        </div>
                                    </div>
                                </div>
                                <h5>Permanent Address</h5>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group{{ $errors->has('street') ? ' has-error' : '' }}">
                                            <label class="control-label">Street</label>
                                            <input name="street" value="{{ old('street') }}" type="text" required class="form-control">
                                            <small class="text-danger">{{ $errors->first('street') }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('district') ? ' has-error' : '' }}">
                                            <label class="control-label">District</label>
                                            <input name="district" value="{{ old('district') }}" required type="text" class="form-control">
                                            <small class="text-danger">{{ $errors->first('district') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('province') ? ' has-error' : '' }}">
                                            <label>Province</label>
                                            <select name="province" class="form-control" required="required">
                                                <option value="">--Select your Province--</option>
                                                <option value="province1" {{ (old("province") == 'province1' ? "selected":"") }}>Province 1</option>
                                                <option value="province2" {{ (old("province") == 'province2' ? "selected":"") }}>Province 2</option>
                                                <option value="province3" {{ (old("province") == 'province3' ? "selected":"") }}>Province 3</option>
                                                <option value="province4" {{ (old("province") == 'province4' ? "selected":"") }}>Province 4</option>
                                                <option value="province5" {{ (old("province") == 'province5' ? "selected":"") }}>Province 5</option>
                                                <option value="province6" {{ (old("province") == 'province6' ? "selected":"") }}>Province 6</option>
                                                <option value="province7" {{ (old("province") == 'province7' ? "selected":"") }}>Province 7</option>
                                            </select>
                                            <small class="text-danger">{{ $errors->first('province') }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('post_code') ? ' has-error' : '' }}">
                                            <label class="control-label">Post Code</label>
                                            <input type="number" value="{{ old('post_code') }}" name="post_code" class="form-control" required>
                                            <small class="text-danger">{{ $errors->first('post_code') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('country') ? ' has-error' : '' }}">
                                            <label>Country</label>
                                            <select name="country" class="form-control" required="required">
                                                <option value="">--Select your Country--</option>
                                                <option value="nepal" {{ (old("country") == 'nepal' ? "selected":"") }}>Nepal</option>
                                                <option value="india" {{ (old("country") == 'india' ? "selected":"") }}>India</option>
                                                <option value="srilanka" {{ (old("country") == 'srilanka' ? "selected":"") }}>Sri Lanka</option>
                                                <option value="usa" {{ (old("country") == 'usa' ? "selected":"") }}>USA</option>
                                            </select>
                                            <small class="text-danger">{{ $errors->first('country') }}</small>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- Step 3 -->
                            <h6>Official Info</h6>
                            <section>
                                <hr>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('department_id') ? ' has-error' : '' }}">
                                            <label for = "department_id">Department</label>
                                            <select name="department_id" id = "department_id" class="form-control" required="required">
                                                <option value="">----select department----</option>
                                                @foreach($departments as $department)
                                                    <option value = "{{ $department->id }}"  {{ (old("department_id") == $department->id ? "selected":"") }}>{{$department->name}}</option>
                                                @endforeach
                                            </select>
                                            <small class="text-danger">{{ $errors->first('department_id') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('position') ? ' has-error' : '' }}">
                                            <label class="control-label">Position</label>
                                            <input name="position" value="{{ old('position') }}" type="text" class="form-control" required>
                                            <small class="text-danger">{{ $errors->first('position') }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="control-label">Date of Joining</label>
                                        <div class="input-group">
                                            <input type="text" name="join_date" value="{{ old('join_date') }}" class="form-control" id="datepicker-autoclose" required placeholder="mm/dd/yyyy">
                                            <div class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                                    </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('salary') ? ' has-error' : '' }}">
                                            <label class="control-label">Salary</label>
                                            <input name="salary" value="{{ old('salary') }}" type="number" required class="form-control">
                                            <small class="form-control-feedback"> per month. </small>
                                            <small class="text-danger">{{ $errors->first('salary') }}</small>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Pan Number</label>
                                            <input name="pan_number" value="{{ old('pan_number') }}" type="string" class="form-control">
                                        </div>
                                    </div>
{{--                                    <div class="col-md-6">--}}
{{--                                        <label class="control-label">Upload User Contract paper</label>--}}
{{--                                        <div class="custom-file mb-3">--}}
{{--                                            <input type="file" name="contract_paper" class="custom-file-input" id="customFile3">--}}
{{--                                            <label class="custom-file-label form-control" for="customFile3">Choose file</label>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group{{ $errors->has('bank_name') ? ' has-error' : '' }}">
                                            <label class="control-label">Bank Name</label>
                                            <input type="string" name="bank_name" value="{{ old('bank_name') }}" required class="form-control">
                                            <small class="text-danger">{{ $errors->first('bank_name') }}</small>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label">Bank Account Number</label>
                                            <input type="string" name="bank_account_number" value="{{ old('bank_account_number') }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <!-- Step 4 -->
                            <h6>Responsibility</h6>
                            <section>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12{{ $errors->has('responsibility') ? ' has-error' : '' }}">
                                        <label for="">Add Responsibility</label>
                                        <textarea type="text" name="responsibility" required class="form-control summernote">{{ old('responsibility') }}</textarea>
                                        <small class="text-danger">{{ $errors->first('responsibility') }}</small>
                                    </div>
                                </div>
                            </section>
                            <div class="form-actions m-t-20">
                                <button type="submit" id="submitBtn" class="btn btn-success"> Submit </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>


{{--<div class="container">--}}
{{--<div class="row justify-content-center">--}}
{{--    <div class="col-md-8">--}}
{{--        <div class="card">--}}
{{--            <div class="card-header">{{ __('Register') }}</div>--}}

{{--            <div class="card-body">--}}
{{--                <form method="POST" action="{{ route('register') }}">--}}
{{--                    @csrf--}}

{{--                    <div class="form-group row">--}}
{{--                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                            @error('name')--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="form-group row">--}}
{{--                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                            @error('email')--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="form-group row">--}}
{{--                         <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('Address') }}</label>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <input id="address" type="text" class="form-control @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus>--}}

{{--                            @error('address')--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="form-group row">--}}
{{--                        <label for="contact_num" class="col-md-4 col-form-label text-md-right">{{ __('Contact No.') }}</label>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <input id="contact_num" type="text" class="form-control @error('contact_num') is-invalid @enderror" name="contact_num" value="{{ old('contact_num') }}" required autocomplete="contact_num" autofocus>--}}

{{--                            @error('contact_num')--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="form-group row">--}}
{{--                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                            @error('password')--}}
{{--                            <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="form-group row">--}}
{{--                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>--}}

{{--                        <div class="col-md-6">--}}
{{--                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="form-group row mb-0">--}}
{{--                        <div class="col-md-6 offset-md-4">--}}
{{--                            <button type="submit" class="btn btn-primary">--}}
{{--                                {{ __('Register') }}--}}
{{--                            </button>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </form>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--</div>--}}

    {{Form::close()}}
@endsection
