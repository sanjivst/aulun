@extends('admin.layout.layout')
@section('content')
		    <div class="row page-titles">
		        <div class="col-12">
		            <div class="card">
		                <div class="card-body">
		                    <h4 class="card-title">List of User</h4>
                            <hr>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered">
                                    <thead class="tbl-th-title">
                                        <tr>
                                            <th>S.N</th>
                                            <th>Name</th>
                                            <th>Temporary Address</th>
                                            <th>Phone Number</th>
                                            <th>Email</th>
                                            <th>Department</th>
                                            <th>Position</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php($i = 1)
                                        @foreach($users as $user)
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $user->full_name }}</td>
                                            <td>{{ $user->temp_address }}</td>
                                            <td>{{ $user->contact_one }}</td>
                                            <td>{{ $user->email }}</td>
                                            <td>{{ $user->department->name }}</td>
                                            <td>{{ $user->position }}</td>
                                            <td>
                                            <a href = "{{ route('user.edit', $user->id) }}" class = "btn btn-success btn-sm">Edit</a>
                                            <a href = "{{ route('user.destroy', $user->id) }}" class = "btn btn-danger btn-sm">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
		                </div>
		            </div>
		        </div>
		    </div>
@endsection
