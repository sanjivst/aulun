@extends('user.layout.layout')
@section('user-content')
  <div class="row page-titles">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Agreements</h4>
                <hr>
                @php($i = 1)
                @foreach($agreements as $agreement)
                <button type="button" name="button" data-toggle="modal" data-target="#exampleModalLong">Contract No. {{ $i++ }}</button>
                <!-- Modal Starts -->
                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Agreement Paper</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                         <div class="modal-body">
                          <embed src="/files/{{ $agreement->agreement }}" type="application/pdf" frameborder="0" width="100%" height="400px">
                        </div>
                      </div>
                  </div>
                </div>
                <!-- Modal Ends -->
                @endforeach

            </div>
        </div>
    </div>
  </div>
@endsection
