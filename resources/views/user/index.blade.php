@extends('user.layout.layout')

@section('user-content')
		    <div class="row page-titles">
		        <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">List of Specific Task</h4>
                            <hr>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered">
                                    <thead class="tbl-th-title">
                                        <tr>
                                            <th>S.N</th>
                                            <th>Start & End Date</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Priority</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody class="tbl-th-info">
                                        @php($i = 1)
                                        @foreach($tasks as $task)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{$task->date_and_time}}</td>
                                            <td>{{$task->title}}</td>

                                            <!-- Button trigger modal -->
                                            <td>
                                                <button type="button" class="btn btn btn sm" data-toggle="modal" data-target="#exampleModalLong">
                                                Description
                                                </button>

                                                <!-- Modal -->
                                                <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLongTitle">Task Description</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                {!!html_entity_decode($task->description)!!}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>


                                            <!-- <td><span class="badge badge-success badge-size">{{$task->priority}}</span></td> -->
																						<!-- Priority -->
																								@if( $task->priority == 'high')
																								<td><span class="badge badge-danger badge-size">High</span></td>
																								@elseif( $task->priority == 'medium')
																								<td><span class="badge badge-primary badge-size">Medium</span></td>
																								@else( $task->priority == 'low')
																								<td><span class="badge badge-success badge-size">Low</span></td>
																								@endif
                                            <td>
                                                <div class="form-group m-b-0">
                                                    <form action="{{url('user/change-task-status/'.$task->id)}}" method="post">
                                                        @csrf
                                                        <select name="status" id="" class="form-control">
                                                            <option value="0" {{$task->status == 0 ? 'selected' : ''}}> Pending</option>
                                                            <option value="1" {{$task->status == 1 ? 'selected' : ''}}> Processing</option>
                                                            <option value="2" {{$task->status == 2 ? 'selected' : ''}}> Completed</option>
                                                        </select>

                                                </div>
                                            </td>
                                            <td>
                                                <input type="submit" class="btn btn-info">
																						 </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Write Notes</h4>
                            <hr>
                            {{Form::open(['method' => 'post', 'route' => 'usernote.store', 'enctype' => 'multipart/form-data'])}}

                                <div class="form-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label for="">Description</label>
                                            <textarea name="description" class="summerNotes form-control">
																						</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions m-t-20">
                                    <button type="submit" class="btn btn-info"> Submit </button>
                                </div>
                                {{Form::close()}}
                                <hr>
                            <h4 class="card-title">List Of Notes</h4>
                            <div class="table-responsive">
                                <table id="myTable" class="table table-bordered">
                                    <thead class="tbl-th-title">
                                        <tr>
                                            <th>S.N</th>
                                            <th>Date & Time</th>
                                            <th>Notes</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <thead class="tbl-th-info">
                                        @php($i = 1)
                                        @foreach($usernotes as $usernote)
                                        <tr>
                                            <td>{{$i++}}</td>
                                            <td>{{ $usernote->updated_at }}</td>
                                            <td>
                                                <a href="#" data-toggle="modal" data-target="#noteInfo">
                                                    {{$usernote->description}}
                                                </a>
                                            </td>
                                            <td class="table-btn-circle">
                                            <a href = "{{route('usernote.edit',$usernote->id)}}" class = "btn btn-success btn-sm">Edit</a>
                                            <a href = "{{route('usernote.destroy',$usernote->id)}}" class = "btn btn-danger btn-sm">Delete</a>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>


		           	<div class="card">
		                <div class="card-body">
		                    <h4 class="card-title">Responsibilities</h4>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    {!!html_entity_decode(Auth::user()->responsibility)!!}
                                </div>
                            </div>
                            </hr>
		                </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <nav class="sidebar-nav" align="center" >
                                <ul id="sidebarnav">
                                    <li>
                                        <a href="{{route('allAttendance.list')}}" aria-expanded="false">
                                            <i class="mdi mdi-bullseye"></i>
                                            <span class="hide-menu">See Attendance List </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="has-arrow " href="{{route('leaveApplication.create')}}" aria-expanded="false">
                                            <i class="mdi mdi-bullseye"></i>
                                            <span class="hide-menu">Leave Application</span>       
                                        </a>
                                        
                                    </li>
                                    <li>
                                        <a class="has-arrow " href="#" aria-expanded="false">
                                            <i class="mdi mdi-bullseye"></i>
                                            
                                            <span class="hide-menu">Holidays</span>
                                        </a>
                                        
                                        <ul aria-expanded="false" class="collapse">
                                            <li><a href="{{route('show.agreement')}}"></a></li>
                                            <i class="mdi mdi-bullseye"></i>
                                            
                                            <span class="hide-menu">Your Agreements</span>
                                        </ul>
                                    </li>
                                    
                                    
                                </ul>
                            </nav>
                    
                        </div>
                    </div>


		        </div>
		    </div>
@endsection
