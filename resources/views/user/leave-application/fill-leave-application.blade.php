
@extends('user.layout.layout')
@section('user-content')


				{{Form::open(['method' => 'post', 'route' => 'leaveApplication.store', 'enctype' => 'multipart/form-data'])}}
					<div class="row page-titles">
						<div class="col-12">
							<div class="card">
								<div class="card-body">
									<h4 class="card-title">Add Leave-application</h4>
									<hr>
									
									<div class="form-group">
										<label>Reason for Leave</label>
										<select name="reason" class="form-control"> 
											<option>select option</option>
											<option>Vacation</option>
											<option>Sick-self</option>
											<option>Wedding</option>
											<option>Sick-family</option>
											<option>Maternity</option>
											<option>Funeral</option>
											<option>Other</option>
										</select>
									</div>

										<div class="form-group">
											<label>Start Date</label>
											<input type="date" name = "startdate"  class="form-control" required>
										</div>

										<div class="form-group">
											<label>End Date</label>
											<input type="date" class="form-control " name = "enddate" required>
										</div>

										<div class="form-group">
											<label>Notes/Comment</label>
											<input type="text" class="form-control " name = "note" placeholder="Your Notes here.." required>
										</div>


										<button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
									</form>
								</div>
							</div>
						</div>
					</div>

				
				{{Form::close()}}
	
			

	
@endsection

