@extends('user.layout.layout')
@section('user-content')

<div class="row page-titles">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="col-md-6">
                    <a class="btn btn-info" href="{{ route('leaveApplication.index') }}" role="button">Fill Leave Application </a>  
                </div> 

                <div class="col-md-6">
                    <p>Your Recent Leave-application has been  {{$leave->response}}.</p>
                </div>
        </div>
    </div>
    </div>
</div>
@endsection