@extends('user.layout.layout')
@section('user-content')
<div class="card">
      <div class="card-body">
          <h4 class="card-title">Edit User Note</h4>
          <hr>
          {{Form::open(['method' => 'patch', 'route' => ['usernote.update',$usernote->id], 'enctype' => 'multipart/form-data'])}}

              <div class="form-body">
                  <div class="row">
                      <div class="col-md-12">
                          <label for="">Description</label>
                          <textarea name="description" class="form-control">{{$usernote->description}}</textarea>
                      </div>
                  </div>
              </div>
              <div class="form-actions m-t-20">
                  <button type="submit" class="btn btn-success"> Submit </button>
              </div>
              {{Form::close()}}
      </div>
</div>
@endsection
