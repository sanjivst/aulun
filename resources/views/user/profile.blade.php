@extends('user.layout.layout')

@section('user-content')


{{--    <div class="page-wrapper p-t-70">--}}
{{--    @if(session()->has('error'))--}}
{{--        <div class="alert alert-danger">--}}
{{--            <p>{{ session()->get('error') }}</p>--}}
{{--        </div>--}}
{{--    @endif--}}

{{--        @if(session()->has('success'))--}}
{{--            <div class="alert alert-success">--}}
{{--                <p>{{ session()->get('success') }}</p>--}}
{{--            </div>--}}
{{--        @endif--}}


        <div class="container-fluid">
		    <div class="row p-t-20">
                <div class="col-lg-4 col-xlg-3 col-md-5">
                    <div class="card">
                        <div class="card-body p-t-0 p-b-0">
                            <center class="m-t-20">
                                <img src="../images/users/5.jpg" class="img-circle" width="150" />
                                <h4 class="card-title m-t-10">{{ Auth::user()->full_name }}</h4>
                                <h6 class="card-subtitle">{{ Auth::user()->position }}</h6>
                            </center>
                        </div>
                        <hr>
                        <div class="card-body p-t-0">
                            <small class="text-muted">Email address </small>
                            <h6>{{ Auth::user()->email }}</h6>
                            <small class="text-muted db">Phone</small>
                            <h6>{{ Auth::user()->contact_one }}</h6>
                            <small class="text-muted db">Address</small>
                            <h6>{{ Auth::user()->temp_address }}</h6>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-xlg-9 col-md-7">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title">Change Password</h4>
                            <hr>
                            {{Form::open(['method'=>'post', 'route'=>['change.password',$user->id], 'enctype' => 'multipart/form-data'])}}
                                <div class="form-body">
                                    <div class="row col-md-12">
                                        <div class="form-group">
                                            <label class="control-label">Current Password</label>
                                            <input type="password" name="current-password" id="currentPassword" class="form-control" placeholder="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">New Password</label>
                                                <input type="password" name="new-password" id="newPassword" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Confirm Password</label>
                                                <input type="password" name="confirm-password" id="conPassword" class="form-control" placeholder="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions m-t-20">
                                    <button type="submit" class="btn btn-success"> Update </button>
                                </div>
                            {{Form::close()}}
                        </div>
                    </div>
                </div>
            </div>
		</div>
@endsection
