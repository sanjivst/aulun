<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\LeaveApplication;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

//Route for test view

Route::get('/test', function () {
    return view('');
});

Auth::routes();

//user

Route::middleware(['user'])->prefix('user')->group(function () {
    Route::get('/', 'User\ProfileController@index')->name('user.dashboard');
    Route::get('/profile', 'User\ProfileController@profile')->name('user.profile');
    Route::post('/change-password/{user}', 'User\ProfileController@changePassword')->name('change.password');
    Route::post('/change-task-status/{task}', 'User\ProfileController@changeTaskStatus');

    //User's Note
    Route::post('/store-usernote', 'User\UserNoteController@store')->name('usernote.store');
    Route::get('/edit-usernote/{usernote}', 'User\UserNoteController@edit')->name('usernote.edit');
    Route::patch('/update-usernote/{usernote}', 'User\UserNoteController@update')->name('usernote.update');
    Route::get('/destroy-usernote/{usernote}', 'User\UserNoteController@destroy')->name('usernote.destroy');

    //Leave Application
    Route::get('/create-leaveApplication', 'User\LeaveApplicationController@create')->name('leaveApplication.create');
    Route::get('/index-leaveApplication', 'User\LeaveApplicationController@index')->name('leaveApplication.index');
    Route::post('/store-leaveApplication', 'User\LeaveApplicationController@store')->name('leaveApplication.store');
    Route::get('/accept-LeaveApplication/{leaveapplication}', 'User\LeaveApplicationController@accept')->name('leaveApplication.accept');

    //Show agreement
    Route::get('/show-agreement', 'User\AgreementController@show')->name('show.agreement');

    //Attendance
    Route::get('/list-all-user-attendance', 'User\AttendanceController@create')->name('allAttendance.list');

});

//Admin
Route::middleware(['admin'])->prefix('admin')->group(function () {

    //Login
    Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

    Route::get('/', 'Admin\ProfileController@index')->name('admin.dashboard');

    //Profile
    Route::get('/profile', 'Admin\ProfileController@profile')->name('profile');
    Route::patch('/profile/{admin}', 'Admin\ProfileController@update')->name('profile.update');

    //User
    Route::get('/create-user', 'Admin\UserController@create')->name('user.create');
    Route::post('/store-user', 'Admin\UserController@store')->name('user.store');
    Route::get('/show-user', 'Admin\UserController@show')->name('user.show');
    Route::get('/edit-user/{user}', 'Admin\UserController@edit')->name('user.edit');
    Route::patch('/update-user/{user}', 'Admin\UserController@update')->name('user.update');
    Route::get('/destroy-user/{user}', 'Admin\UserController@destroy')->name('user.destroy');


    //Department
    Route::get('/add-department', 'Admin\DepartmentController@create')->name('department.create');

    Route::post('/store-department', 'Admin\DepartmentController@store')->name('department.store');
    Route::get('/show-department', 'Admin\DepartmentController@show')->name('department.show');
    Route::get('/edit-department/{department}', 'Admin\DepartmentController@edit')->name('department.edit');
    Route::patch('/update-department/{department}', 'Admin\DepartmentController@update')->name('department.update');
    Route::get('/destroy-department/{department}', 'Admin\DepartmentController@destroy')->name('department.destroy');

    //Task
    Route::get('/add-task', 'Admin\TaskController@create')->name('task.create');
    Route::post('/store-task', 'Admin\TaskController@store')->name('task.store');
    Route::get('/getEmployee/{department_id}', 'Admin\DepartmentController@getEmployee');
    Route::get('/edit-task/{task}', 'Admin\TaskController@edit')->name('task.edit');
    Route::patch('/update-task/{task}', 'Admin\TaskController@update')->name('task.update');
    Route::get('/destroy-task/{task}', 'Admin\TaskController@destroy')->name('task.destroy');

    //Note
    Route::post('/store-note', 'Admin\NoteController@store')->name('note.store');
    Route::get('/edit-note/{note}', 'Admin\NoteController@edit')->name('note.edit');
    Route::patch('/update-note/{note}', 'Admin\NoteController@update')->name('note.update');
    Route::get('/destroy-note/{note}', 'Admin\NoteController@destroy')->name('note.destroy');

    //Announcement
    Route::get('/add-announcement', 'Admin\AnnouncementController@create')->name('announcement.create');
    Route::post('/store-announcement', 'Admin\AnnouncementController@store')->name('announcement.store');


    //Leave Application
    Route::get('/list-leaveApplication', 'Admin\LeaveApplicationController@index')->name('leaveApplication.list');
    Route::post('/submitResponse-leaveApplication/{leave}', 'Admin\LeaveApplicationController@store')->name('submitResponse');

    //Attendance
    Route::get('/userList-Attendance', 'Admin\AttendanceController@create')->name('attendance.userList');
    Route::get('/fillForm-Attendance/{user_id}', 'Admin\AttendanceController@index')->name('attendance.fillForm');
    Route::post('/store-Attendance/{user_id}', 'Admin\AttendanceController@store')->name('attendance.store');
    Route::get('/list-Attendance/{user_id}', 'Admin\AttendanceController@attendanceList')->name('attendance.list');



    //Agreement
    Route::get('/agreement', 'Admin\AgreementController@index')->name('agreement.index');
    Route::get('/add-agreement/{user}', 'Admin\AgreementController@create')->name('agreement.create');
    Route::post('/store-agreement/{user}', 'Admin\AgreementController@store')->name('agreement.store');
    Route::get('/show-agreement/{user}', 'Admin\AgreementController@show')->name('agreement.show');

    //Events
    Route::get('/add-event', 'Admin\EventController@addEvent')->name('events.page');
    Route::resource('/events','Admin\EventController');

});
